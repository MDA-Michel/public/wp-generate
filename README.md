## WP-generate
### Cloning into the Github repository

In order to clone into the this Gitlab repository, use the following command on your terminal environment

`git clone gitlab.com/MDA-Michel/persoonlijke-projecten/2020/wp-generate` 

After the repository is cloned, you should go into the newly created git directory, from here
on you can ask for additional help on how to use this tool.
`python3 Main.py -h` to show additional help.

![cloning and showing help menu](https://media.giphy.com/media/BPqzuL3Ljk5BW5sD3v/giphy.gif)

### Installing all prerequisites

All needed prerequisites can be installed with the setup.py python3 file, run the following command to install all prerequisites 

`python3 Setup.py` 

![Installing all prerequisites](https://media.giphy.com/media/4xZmzEI8TJG975fvzM/giphy.gif)

### Using WP-generate

After all needed prerequisites are installed, you should be able to run the tool. Within this script you can add as many positional parameters as you'd like.
For example;

When executing the command `python3 Main.py user1 user2` 2 installations will be done of the **latest wordpress version**. However, it is also possible to add even more users/names to the script. The tool will continue on untill all given positional parameters have their own respective installation. So for example, the command `python3 Main.py user1 user2 user3 user4` will all contain their own installation of the latest Wordpress verison.

![Using the tool](https://media.giphy.com/media/jtm1QptFksyfVh51c5/giphy.gif)

### Database credentials

After the tool is ran, database credentials will be generated. These credentials of the database user can be found back in the **output** directory of this tool.

### Important notes

- If Wordpress has been installed in a previous usage of WP-generate, and it is saved in the temp/ directory (created by wp-generate upon usage) it will be reused. Delete the temp/ directory if you want the latest Wordpress version installed 
- Tested on Ubuntu 20.04 

