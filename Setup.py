#!/usr/bin/python3
import os
#
shell = os.system

class Setup:
    def __init__(self):
        print("Installing all prerequisites... this might take a moment")
        shell("apt update -y")
        shell("apt install python3-pip php php-mysql mysql-server apache2 php-curl php-gd php-mbstring php-xml php-xmlrpc php-soap php-intl php-zip -y")
        shell("pip3 install colorama")
        shell("pip3 install requests")
        shell("pip3 install shutil")
        shell("pip3 install argparse")

if __name__ == "__main__":
    Setup()
