from assets.notifications.Notifications import *
#
import sys
import argparse

class Help:
    def __init__(self):
        parser = argparse.ArgumentParser(description="Generate a Wordpress installation for 1 or more then 1 user. \nSince the scripts requires sudo privileges, make sure you run as sudo user.")
        parser.add_argument('user user1 user2', help='specify the user(s) to create the Wordpress website for')
        parser.add_argument('--nologo', help="don't draw the logo at the beginning of the script", dest='')
        parser.parse_args()
