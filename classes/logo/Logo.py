from assets.styling.Color import *
#
import time
# what was used to create the logo :
# http://patorjk.com/software/taag/#p=display&v=2&f=ANSI%20Shadow&t=upload%20-%20geek 

class Logo:
    def __init__(self):
        logo = str("""
██╗    ██╗██████╗        ██████╗ ███████╗███╗   ██╗███████╗██████╗  █████╗ ████████╗███████╗
██║    ██║██╔══██╗      ██╔════╝ ██╔════╝████╗  ██║██╔════╝██╔══██╗██╔══██╗╚══██╔══╝██╔════╝
██║ █╗ ██║██████╔╝█████╗██║  ███╗█████╗  ██╔██╗ ██║█████╗  ██████╔╝███████║   ██║   █████╗  
██║███╗██║██╔═══╝ ╚════╝██║   ██║██╔══╝  ██║╚██╗██║██╔══╝  ██╔══██╗██╔══██║   ██║   ██╔══╝  
╚███╔███╔╝██║           ╚██████╔╝███████╗██║ ╚████║███████╗██║  ██║██║  ██║   ██║   ███████╗
 ╚══╝╚══╝ ╚═╝            ╚═════╝ ╚══════╝╚═╝  ╚═══╝╚══════╝╚═╝  ╚═╝╚═╝  ╚═╝   ╚═╝   ╚══════╝
        """
        )
        self.line = ""
        # Logo is splitted in lines
        self.logo = logo.splitlines()

        # Characters used to create the logo and copyright
        self.alphabetList = [
                        "a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z",
                        "A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z",
                        ]
        self.integerList = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0"]
        self.specialCharList = [ ">", "<", "|", "[", "]", ".", "/", "\\", "_", "(", ")","-", "©", "╝", "╗","═","╔","█","╚","║", " ", "\t"]
        #
        self.drawLogo()
        self.drawCopyRight()
        time.sleep(1)

        # Function that is used to draw the logo character by character.
    def drawLines(self,logoLine, array, line, colour, speed):
        for firstChar in logoLine:
            for secondChar in array:
                line = line + str(secondChar) if str(firstChar) == str(secondChar) else line
                if firstChar != secondChar:
                    print(colour + line + secondChar + reset, end="\r")
                    time.sleep(speed)

        # Draws the logo
    def drawLogo(self):
        colour = [reset, brightRed, red, blue, green, brightGreen, yellow, reset]
        for i in range(len(self.logo)):
            self.drawLines(self.logo[i], self.specialCharList, self.line, colour[i], 0.0001)
            print("")

    # draws the copyright
    def drawCopyRight(self):
        text = str("Corrosie © | Gitlab.com/MDA-Michel | Oktober 2020")
        self.drawLines(text, self.alphabetList + self.integerList + self.specialCharList, self.line, brightRed, 0.002)
        print("\n")

