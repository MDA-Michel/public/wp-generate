from assets.datetime.DateTime import *
from assets.notifications.Notifications import *
#
import os
import hashlib
import random

class Database:
    def __init__(self, user, id):
        self.dbPasswordRoot = input("{time}{importantInfo} Please specify your database password of the user 'root' (if there is no password set press enter)\n".format(
            time=currentTimeFormatted(),
            importantInfo=importantInfo
        ))
        self.dbUser = user
        self.dbName = str("DB_{user}".format(
            id=id,
            user=self.dbUser
        ))
        # create database password
        randomValue = str("{}{}{}".format(random.randint(-9999999, 9999999), datetime.datetime.now(), self.dbUser)).encode('utf-8')
        self.dbPassword = str(hashlib.md5(randomValue).hexdigest())
        #
        os.system("service mysql start")
        os.system("service apache2 start")
        #
        print("{time}{importantInfo} Creating a database, database user and granting privileges to the database user...".format(
            time=currentTimeFormatted(),
            importantInfo=importantInfo
        ))
        #
        self.createDatabase()
        self.createDatabaseUser()
        self.grantDatabasePrivileges()
        self.writeConnectionDetails()
        
    # creates the database for the user
    def createDatabase(self):
        createDatabaseQuery = str("CREATE DATABASE {dbName}".format(
            dbName=self.dbName
        ))
        self.submitQuery(createDatabaseQuery)
    
    # creates the DB user
    def createDatabaseUser(self):
        createDbUserQuery = str("CREATE USER '{dbUser}'@'%' IDENTIFIED WITH mysql_native_password BY '{dbPassword}'".format(
            dbUser=self.dbUser,
            dbPassword=self.dbPassword
        ))
        self.submitQuery(createDbUserQuery)

    # grants all privileges to the newly created db user
    def grantDatabasePrivileges(self):
        grantAllPrivilegesQuery = str("GRANT ALL ON {dbName}.* TO '{dbUser}'@'%'".format(
            dbName=self.dbName,
            dbUser=self.dbUser,
        ))
        flushPrivilegesQuery = str("FLUSH PRIVILEGES")
        #
        self.submitQuery(grantAllPrivilegesQuery)
        self.submitQuery(flushPrivilegesQuery)

    # function for executing MySQL queries
    def submitQuery(self, query):
        if len(self.dbPasswordRoot) > 0:
            os.system('mysql -u root -p "{password}" -e "{query}";'.format(
                password=self.dbPasswordRoot,
                query=query
            ))
        else:
            os.system('mysql -u root -e "{query}";'.format(
            query=query
            ))

    # details needed to make a connection to the database
    def getConnectionDetails(self):
        self.connectionDetails = { 
                "dbname" : self.dbName,
                "uname" : self.dbUser,
                "pwd" : self.dbPassword,
                "dbhost" : "localhost",
                "prefix" : "wp_",
                "language" : "",
                "submit" : "Submit"
        }
        return self.connectionDetails

    def writeConnectionDetails(self):
        connectionDetails = self.getConnectionDetails()
        if os.path.exists('output'):
            f = open("output/database-credentials-{dbUser}.txt".format(dbUser=self.dbUser), "a")
            f.write("##### Database credentials {dbUser} (user:pass) \n\n".format(dbUser=self.dbUser))
            f.write("{user}:{pwd}\n".format(
                user=connectionDetails["uname"],
                pwd=connectionDetails["pwd"]
                ))
            f.close
        else:
            os.mkdir("output")
            f = open("output/database-credentials-{dbUser}.txt".format(dbUser=self.dbUser), "a")
            f.write("##### Database credentials {dbUser} (user:pass) \n\n".format(dbUser=self.dbUser))
            f.write("{user}:{pwd}\n".format(
                user=connectionDetails["uname"],
                pwd=connectionDetails["pwd"]
                ))
            f.close



