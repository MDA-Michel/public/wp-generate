from assets.notifications.Notifications import *
from assets.datetime.DateTime import *
#
import os
import shutil
import requests

class Wordpress:
    def __init__(self, user, id, connectionDetails):
        self.user = user
        self.id = id
        self.connectionDetails = connectionDetails
        #
        print("{time}{importantInfo} Current user '{by1}{user}{r1}' ".format(
            time=currentTimeFormatted(),
            importantInfo=importantInfo,
            user=self.user,
            by1=brightYellow,
            r1=reset
        ))
        #
        self.downloadWordpress()
        self.moveWordpress()
        self.extractWordpress()
        self.configurePermissions()
        self.postRequest()
        #
        print("{time}{importantInfo} Installation finished for user '{by1}{user}{r1}'".format(
            time=currentTimeFormatted(),
            importantInfo=importantInfo,
            user=self.user,
            by1=brightYellow,
            r1=reset
        ))
        print("{time}{importantInfo} Website can be found at '{by1}http://localhost:80/{user}/wordpress'{r1}".format(
            time=currentTimeFormatted(),
            importantInfo=importantInfo,
            user=self.user,
            by1=brightYellow,
            r1=reset
        ))
        #
        print("{time}{importantInfo} Database details can be found at '{by1}output/{user}{r1}'".format(
            time=currentTimeFormatted(),
            importantInfo=importantInfo,
            user=self.user,
            by1=brightYellow,
            r1=reset
        ))
        
    def downloadWordpress(self):
        if os.path.exists("temp/latest.tar.gz"):
            pass
        else:
            print("{time}{importantInfo} Downloading latest Wordpress version, this may take a little moment".format(
                time=currentTimeFormatted(),
                importantInfo=importantInfo
            ))
            wordpress = requests.get("https://wordpress.org/latest.tar.gz", allow_redirects=True)
            open('latest.tar.gz', 'wb').write(wordpress.content)

    def moveWordpress(self):
        if os.path.exists("temp/latest.tar.gz"):
            pass
        else:
            print("{time}{importantInfo} Creating temporary directory '{by1}temp{r1}', Wordpress tarfile will be moved to the temp directory ".format(
                time=currentTimeFormatted(),
                importantInfo=importantInfo,
                by1=brightYellow,
                r1=reset
            ))
            os.system("mkdir -p temp")
            os.system("mv latest.tar.gz temp/latest.tar.gz")

    def extractWordpress(self):
        documentRoot = "/var/www/html" if os.path.exists('/var/www/html') else input("{time}{warning} Default path could not be found ('/var/www/html'), please specify the path \n".format(
            time=currentTimeFormatted(),
            warning=warning
        ))
        result = False
        while result == False:
            if os.path.exists(documentRoot):
                result = True
            else:
                documentRoot = input("{time}{warning} Path '{by1}{documentRoot}{r1}' could not be found, please specify the documentRoot directory! \n".format(
                    time=currentTimeFormatted(),
                    warning=warning,
                    by1=brightYellow,
                    documentRoot=documentRoot,
                    r1=reset
                ))
        self.wordpressDirectory = str("{documentRoot}/{user}".format(
            documentRoot=documentRoot,
            user=self.user
        ))
        os.system("mkdir -p {wordpressDirectory}".format(
            wordpressDirectory=self.wordpressDirectory
        ))
        os.system("tar -xf temp/latest.tar.gz -C {wordpressDirectory}".format(
            wordpressDirectory=self.wordpressDirectory
        ))
    
    def configurePermissions(self):
        print("{time}{importantInfo} Configuring correct permissions/groups after installation".format(
                time=currentTimeFormatted(),
                importantInfo=importantInfo,
            ))
        os.system("sudo chown -R www-data:www-data {wordpressDirectory}".format(
            wordpressDirectory=self.wordpressDirectory
        ))
        os.system("find {wordpressDirectory} -type d -exec chmod 750 {{}} \;".format(
            wordpressDirectory=self.wordpressDirectory
        ))
        os.system("find {wordpressDirectory} -type f -exec chmod 640 {{}} \;".format(
            wordpressDirectory=self.wordpressDirectory
        ))

    def postRequest(self):
        hostAndUri = str("http://127.0.0.1:80/{dbUser}/wordpress/wp-admin/setup-config.php?step=2".format(
            dbUser=self.user
            ))
        # sends post request, makes an connection to the database
        requests.post("{hostAndUri}".format(
            hostAndUri=hostAndUri),
            data=self.connectionDetails
            )