from colorama import Fore, Style

green = Fore.GREEN
brightGreen = Style.BRIGHT + green

red = Fore.RED
brightRed = Style.BRIGHT + red

blue = Fore.BLUE
brightBlue = Style.BRIGHT + blue

yellow = Fore.YELLOW
brightYellow = Style.BRIGHT + yellow

reset = Style.RESET_ALL
