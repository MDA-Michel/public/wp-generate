### imports
from assets.styling.Color import *
#
import datetime
### variables
# currentDateTime = datetime.datetime.now()
### functions
def currentDateFormatted():
    return str("[{yellow}{year}:{month}:{day}{reset}]".format(
        yellow=yellow,
        year=datetime.datetime.now().year,
        month=datetime.datetime.now().month if len(str(datetime.datetime.now().month)) == 2 else str("0{}".format(datetime.datetime.now().month)),
        day=datetime.datetime.now().day if len(str(datetime.datetime.now().day)) == 2 else str("0{}".format(datetime.datetime.now().day)),
        reset=reset
    ))

def currentTimeFormatted():
    return str("[{blue}{hour}:{minute}:{second}{reset}]".format(
        blue=blue,
        hour=datetime.datetime.now().hour if len(str(datetime.datetime.now().hour)) == 2 else str("0{}".format(datetime.datetime.now().hour)),
        minute=datetime.datetime.now().minute if len(str(datetime.datetime.now().minute)) == 2 else str("0{}".format(datetime.datetime.now().minute)),
        second=datetime.datetime.now().second if len(str(datetime.datetime.now().second)) == 2 else str("0{}".format(datetime.datetime.now().second)),
        reset=reset
    ))

