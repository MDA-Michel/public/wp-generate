from assets.styling.Color import *

basicNotification = str("{}[*]{}".format(green, reset))
basicInfo = str("{}[+]{}".format(brightGreen, reset))
basicWarning = str("{}[*]{}".format(red, reset))
basicError = str("{}[+]{}".format(brightRed, reset))

info = str("[{}INFO{}]".format(green, reset))
importantInfo = str("[{}INFO{}]".format(brightGreen, reset))
warning = str("[{}WARNING{}]".format(red, reset))
error = str("[{}ERROR{}]".format(brightRed, reset))
question = str("[{}Question{}]".format(brightYellow, reset))
match = str("[{}MATCH!{}]").format(brightBlue,reset)
test = str("{}[TEST]{}".format(yellow, reset))