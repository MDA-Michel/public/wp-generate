#!/usr/bin/python3
#
from classes.help.Help import *
from classes.logo.Logo import *
from classes.wordpress.Wordpress import *
from classes.wordpress.database.Database import *
from assets.datetime.DateTime import *
from assets.notifications.Notifications import *
#
import os
import sys
import time
#

class Main:
    def __init__(self):
        if len(sys.argv) >= 2 and str("--help") not in str(sys.argv) and str("-h") not in str(sys.argv):
            if str("--nologo") not in sys.argv:
                Logo()
            users = sys.argv
            print("{time}{importantInfo} Starting up Wordpress auto-install script, make sure you run the script as the user root!".format(
                time=currentTimeFormatted(), 
                importantInfo=importantInfo
                ))
            for user in range(1, len(users)):
                if str("--nologo") == users[user]:
                    continue
                id = user
                print("{basicInfo}[{id}] {student}".format(
                    basicInfo=basicInfo, 
                    id=id, 
                    student=users[user]
                    ))
            time.sleep(1)
            for user in range(1, len(users)):
                if str("--nologo") == users[user]:
                    continue
                id = user
                connectionDetails = Database(users[user], id).getConnectionDetails()
                Wordpress(users[user], id, connectionDetails)
        else: 
            Help()

if __name__ == "__main__":
    Main()
